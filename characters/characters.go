/**
characters.go

Last Modified: August 19, 2019
**/

package characters

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/ContextFreeHuman/eq-progression-tracker/gear"
	"gitlab.com/ContextFreeHuman/eq-progression-tracker/ts"
)

var (
	// CurrentChar represents the character active in the program
	CurrentChar Character

	// aadbs is a slice containing the names of each of the "types" of AAs that exist
	aadbs = []string{"general", "archetype", "class", "special", "focus", "mercenary"}
)

// Character structs represent a player character.
type Character struct {
	Name, Class, Race, Deity, Server string
	Level                            int
	AAs                              []map[int]int
	Recipes                          [][]ts.Recipe
	Gear                             []gear.Equipped
}

// Initialize loads a character into memory using the character struct.
func Initialize(name, class, race, deity, server string, level int, aaIndex map[int]int) (char Character) {
	char.Name, char.Class, char.Race, char.Deity, char.Server = name, class, race, deity, server
	char.Level = level
	char.AAs = ImportCharacter(name, server, aaIndex)
	// TODO: No specific import for recipes yet ... don't try to access that database. :)
	char.Gear = LoadGear(name, server)
	return
}

// ImportCharacter reads a character's AA file (if exists) and loads the IDs and purchased ranks of each into memory.
func ImportCharacter(name, server string, aaIndex map[int]int) (characterAA []map[int]int) {
	characterAA = make([]map[int]int, 6)
	for i := range characterAA {
		characterAA[i] = make(map[int]int, 0)
	}
	aaFile, err := os.Open("characters/" + name + "-" + server + "/" + name + "_aas")
	if err != nil {
		fmt.Printf("Error: Character with name %v does not exist or file is corrupted.", name)
		return
	}
	stepper := bufio.NewScanner(aaFile)
	for stepper.Scan() {
		line := strings.Fields(stepper.Text())
		for _, idStr := range line {
			id, err := strconv.Atoi(idStr)
			if err != nil {
				fmt.Printf("Error: Failed to convert %v to an integer.", idStr)
				err = nil // Continue
			}
			aadb := aaIndex[id]
			rank, exists := characterAA[aadb][id]
			if exists {
				characterAA[aadb][id] = rank + 1
			} else {
				characterAA[aadb][id] = 1
			}
		}
	}
	return
}

// LoadCharacter reads a character's info file (Ex. Test-Test/Test) and creates a struct for it.
func LoadCharacter(path string, aaIndex map[int]int) (status string) {
	index := 0
	for i := 0; i < len(path); i++ {
		if path[i] == '\\' {
			index = i
		}
	}
	path = path[index+1 : len(path)] // Character name with server attached
	for i := 0; i < len(path); i++ {
		if path[i] == '-' {
			index = i
		}
	}
	name := path[0:index]                                   // Character name only
	file, err := os.Open("characters/" + path + "/" + name) // Open basic character file
	if err != nil {
		fmt.Printf("Error: Failed to open file at %v", "characters/"+path+"/"+name)
		return "Failure"
	}
	defer file.Close()
	stepper := bufio.NewScanner(file)
	for stepper.Scan() {
		line := strings.Fields(stepper.Text())
		level, err := strconv.Atoi(line[1])
		if err != nil {
			fmt.Printf("Error: Failed to convert %v to an integer", line[1])
			return "Failure"
		}
		i := 2
		strs := make([]string, 3)
		for j := 0; j < 3; j++ {
			var str string
			for line[i] != ";" {
				str += line[i] + " "
				i++
			}
			strs[j] = str[0 : len(str)-1]
			i++
		}
		CurrentChar = Initialize(line[0], strs[1], strs[0], line[i], strs[2], level, aaIndex)
	}
	return
}

// LoadGear creates the gear slots for a character and populates item information into those slots.
func LoadGear(name, server string) (slots []gear.Equipped) {
	slots = make([]gear.Equipped, 23)

	gearFile, err := os.Open("characters/" + name + "-" + server + "/" + name + "_gear")
	if err != nil {
		fmt.Printf("Error: Failed to open gear info for character %v, on server %v.\n", name, server)
		return
	}
	defer gearFile.Close()
	stepper := bufio.NewScanner(gearFile)
	stepper.Scan()
	line := strings.Fields(stepper.Text())
	lastSlot := 0
	for i := 0; i < len(line); i++ {
		if line[i] == "(" {
			if line[i+1] == ")" {
				slots[lastSlot].Adorns = append(slots[lastSlot].Adorns, gear.Dummy)
				i++
				continue
			} // else
			id, err := strconv.Atoi(line[i+1])
			if err != nil {
				fmt.Printf("Error: Failed to convert %v to an integer.\n", line[i+1])
			}
			slots[lastSlot].Adorns = append(slots[lastSlot].Adorns, gear.AdornDB[id])
			i += 2
			continue
		}

		id, err := strconv.Atoi(line[i])
		if err != nil {
			fmt.Printf("Error: Failed to convert %v to an integer.\n", line[i])
			lastSlot++
			continue
		}
		if id == 0 {
			slots[lastSlot].Status = false
			lastSlot++
			continue
		}
		if lastSlot < 19 {
			slots[lastSlot].EquipA = gear.ArmorDB[id]
		} else {
			if lastSlot < 22 {
				if gear.ItemIndex[id] == "armor" {
					slots[lastSlot].EquipA = gear.ArmorDB[id]
					slots[lastSlot].EquipW = gear.WeaponDB[id]
					slots[lastSlot].EquipW.Info.ID = -1 // Equivalent of setting weapon to NIL
				} else {
					slots[lastSlot].EquipW = gear.WeaponDB[id]
					slots[lastSlot].EquipA = gear.ArmorDB[id]
					slots[lastSlot].EquipA.Info.ID = -1 // Equivalent of setting weapon to NIL
				}
			} else {
				if lastSlot == 22 {
					slots[lastSlot].EquipAm = gear.AmmoDB[id]
				}
			}
		}
		slots[lastSlot].Status = true
		lastSlot++
	}
	return
}

// CreateCharacter creates an info file detailing a new character (if not already existing) and also loads their data into memory.
func CreateCharacter(name, class, race, deity, server string, level int, aaIndex map[int]int) (status string) {
	_, err := os.Stat("characters/" + name + "-" + server + "/" + name)
	if err == nil {
		return "Failure" // TODO: Make this error more explicit
	}
	err = nil // Reset
	err = os.Mkdir("characters/"+name+"-"+server, os.ModeDir)
	if err != nil {
		fmt.Printf("Error: Failed to create new directory %v", "characters/"+name+"-"+server)
		return "Failure"
	}
	infoFile, err := os.Create("characters/" + name + "-" + server + "/" + name)
	if err != nil {
		fmt.Printf("Error: Failed to create new file %v", "characters/"+name+"-"+server+"/"+name)
		return "Failure"
	}
	defer infoFile.Close()
	_, err = os.Create("characters/" + name + "-" + server + "/" + name + "_aas")
	if err != nil {
		fmt.Printf("Error: Failed to create new file %v", "characters/"+name+"-"+server+"/"+name+"_aas")
		return "Failure"
	}
	gearFile, err := os.Create("characters/" + name + "-" + server + "/" + name + "_gear")
	if err != nil {
		fmt.Printf("Error: Failed to create new file %v", "characters/"+name+"-"+server+"/"+name+"_gear")
		return "Failure"
	}
	_, err = os.Create("characters/" + name + "-" + server + "/" + name + "_recipes")
	if err != nil {
		fmt.Printf("Error: Failed to create new file %v", "characters/"+name+"-"+server+"/"+name+"_recipes")
		return "Failure"
	}
	infoFile.WriteString(name + " " + strconv.FormatInt(int64(level), 10) + " " + race + " ; " + class + " ; " + server + " ; " + deity) // Fill out character info file
	for i := 0; i < 23; i++ {
		gearFile.WriteString("0 ")
	}
	CurrentChar = Initialize(name, class, race, deity, server, level, aaIndex)
	return
}

// WriteCharAA opens the current character's AA file and updates the information there.
func WriteCharAA() {
	charAA, err := os.OpenFile("characters/"+CurrentChar.Name+"-"+CurrentChar.Server+"/"+CurrentChar.Name+"_aas", os.O_WRONLY, 0222)
	if err != nil {
		fmt.Printf("Error: Failed to open file %v.\n", "characters/"+CurrentChar.Name+"-"+CurrentChar.Server+"/"+CurrentChar.Name+"_aas")
		return
	}
	defer charAA.Close()
	charAA.Truncate(0)
	output := make([]int, 0)
	for _, db := range CurrentChar.AAs {
		for id, rank := range db {
			for i := 0; i < rank; i++ {
				output = append(output, id)
			}
		}
	}
	sort.Slice(output, func(i, j int) bool {
		return output[i] <= output[j]
	})
	for _, id := range output {
		charAA.WriteString(strconv.FormatInt(int64(id), 10) + " ")
	}
}

// WriteCharInfo opens the current character's info file and updates the information there.
func WriteCharInfo() {
	info, err := os.OpenFile("characters/"+CurrentChar.Name+"-"+CurrentChar.Server+"/"+CurrentChar.Name, os.O_WRONLY, 0222)
	if err != nil {
		fmt.Printf("Error: Failed to open file %v.\n", "characters/"+CurrentChar.Name+"-"+CurrentChar.Server+"/"+CurrentChar.Name)
		return
	}
	defer info.Close()
	info.Truncate(0)
	info.WriteString(CurrentChar.Name + " " + strconv.FormatInt(int64(CurrentChar.Level), 10) + " " + CurrentChar.Race + " ; " + CurrentChar.Class + " ; " + CurrentChar.Server + " ; " + CurrentChar.Deity)
}

// WriteCharGear opens the current character's gear file and updates the information there.
func WriteCharGear() {
	gear, err := os.OpenFile("characters/"+CurrentChar.Name+"-"+CurrentChar.Server+"/"+CurrentChar.Name+"_gear", os.O_WRONLY, 0222)
	if err != nil {
		fmt.Printf("Error: Failed to open file %v.\n", "characters/"+CurrentChar.Name+"-"+CurrentChar.Server+"/"+CurrentChar.Name+"_gear")
		return
	}
	defer gear.Close()
	gear.Truncate(0)
	var str string
	for _, slot := range CurrentChar.Gear {
		if slot.EquipA.Info.ID == 0 && slot.EquipW.Info.ID == 0 && slot.EquipAm.Info.ID == 0 {
			str += "0 "
		} else {
			for _, adorn := range slot.Adorns {
				if adorn.Info.ID == 0 {
					str += "( ) "
				} else {
					str += "( " + strconv.FormatInt(int64(adorn.Info.ID), 10) + " ) "
				}
			}
			if slot.EquipA.Info.ID != 0 && slot.EquipA.Info.ID != -1 {
				str += strconv.FormatInt(int64(slot.EquipA.Info.ID), 10) + " "
			} else {
				if slot.EquipW.Info.ID != 0 {
					str += strconv.FormatInt(int64(slot.EquipW.Info.ID), 10) + " "
				} else {
					str += strconv.FormatInt(int64(slot.EquipAm.Info.ID), 10) + " "
				}
			}
		}
	}
	gear.WriteString(str)
}
