/**
aa.go

Last Modified: January 15, 2020
**/

package aa

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/ContextFreeHuman/eq-progression-tracker/characters"
)

var (
	// AADBs is the database of all known AAs
	AADBs []map[int][]AA
	// AAIndex maps an AA id to its type
	AAIndex map[int]int
	// Flags represents the following: Tradeskill Filter On/Off
	Flags = []bool{false}
	// Filters refers to the "owned", "unowned", and "available" filters, respectively, as booleans. True means the filter is enabled.
	Filters = []bool{true, true, false, true}
	// DisplayedAA holds the AA that should currently be displayed in any given ListStore
	DisplayedAA [][]AA
	// DBs is a slice containing the names of each of the "types" of AAs that exist
	DBs = []string{"general", "archetype", "class", "special", "focus", "mercenary"}
)

// AA represents an Alternate Advancement and its details.
type AA struct {
	ID, Level, Cost, Rank, Timer, Cooldown int
	Name, Category, Description, Notes     string
	Restrictions                           []string // Class restrictions, e.g.
}

// Import collects the list of a particular type of AA and loads it into memory.
func Import(filename string) (aaList map[int][]AA) {
	aas, err := os.Open(filename)
	if err != nil {
		fmt.Printf("Error: Failed to open file at %v", filename)
		return
	}
	defer aas.Close()
	aaList = make(map[int][]AA, 0)
	stepper := bufio.NewScanner(aas)
	for stepper.Scan() { // TODO: Don't let the whole program crash if someone or something tampers with the database format
		line := strings.Fields(stepper.Text())
		var restrictions []string
		ints, strs := make([]int, 6), make([]string, 4)
		i := 0
		for i < 6 {
			num, err := strconv.Atoi(line[i])
			if err != nil {
				fmt.Printf("Error: Failed to convert %v to an integer", line[i])
				err = nil // Continue
			}
			ints[i] = num
			i++
		}
		for j := 0; j < 4; j++ {
			var str string
			for line[i] != ";" {
				str += line[i] + " "
				i++
			}
			if len(str) > 0 {
				strs[j] = str[0 : len(str)-1] // Takes the trailing space off an entry
			} else {
				strs[j] = ""
			}
			i++
		}
		for i != len(line) {
			restrictions = append(restrictions, line[i])
			i++
		}
		aaList[ints[0]] = append(aaList[ints[0]], AA{ints[0], ints[1], ints[2], ints[3], ints[4], ints[5], strs[0], strs[1], strs[2], strs[3], restrictions})
	}
	return
}

// ImportDefault collects the lists from all kinds of AAs and loads them into memory.
func ImportDefault() {
	aaLists := make([]map[int][]AA, 6)
	for i, kind := range DBs {
		aaLists[i] = Import("aa/" + kind + "DB")
	}
	AADBs = aaLists
}

// LoadIndex populates the AA index, mapping AA ids to types.
func LoadIndex() {
	index := make(map[int]int, 0)
	list, err := os.Open("aa/aaIndex")
	if err != nil {
		fmt.Printf("Error: Failed to open file aaIndex")
		return
	}
	defer list.Close()
	stepper := bufio.NewScanner(list)
	for stepper.Scan() {
		line := strings.Fields(stepper.Text())
		id, err := strconv.Atoi(line[0])
		if err != nil {
			fmt.Printf("Error: Failed to convert %v to an integer", line[0])
			err = nil // Continue
		}
		kind, err := strconv.Atoi(line[1])
		if err != nil {
			fmt.Printf("Error: Failed to convert %v to an integer", line[1])
			err = nil // Continue
		}
		index[id] = kind
	}
	AAIndex = index
}

// AvailableAA returns all AA that a character meets the requirements for and has not purchased from a particular category.
func AvailableAA(aaList map[int][]AA, characterAA map[int]int, char characters.Character) (availableAA []AA) {
	availableAA = make([]AA, 0)
	for id, aas := range aaList {
		rank := characterAA[id]
		if rank < len(aas) {
			if aas[rank].Level <= char.Level && RestrictionsMet(aas[rank].Restrictions, char) {
				availableAA = append(availableAA, aas[rank])
			}
		}
	}
	return
}

// AllAvailableAA returns all AA that a character meets the requirements for and has not purchased from all categories.
func AllAvailableAA(char characters.Character) (availableAA [][]AA) {
	characterAAs := char.AAs
	availableAA = make([][]AA, 6)
	for i := range DBs {
		availableAA[i] = AvailableAA(AADBs[i], characterAAs[i], char)
	}
	return
}

// RestrictionsMet determines whether a character satisfies one of the attributes specified by the "restrictions" slice of an AA.
func RestrictionsMet(restrictions []string, char characters.Character) bool {
	if len(restrictions) == 0 {
		return true
	}
	for _, value := range restrictions {
		if char.Class == value {
			return true
		}
		if char.Race == value {
			return true
		}
	}
	return false
}

// ResetDisplayed reinitializes the DisplayedAA slice.
func ResetDisplayed() {
	DisplayedAA = make([][]AA, 6)
	for i := range DisplayedAA {
		DisplayedAA[i] = make([]AA, 0)
	}
}

// ChangeFilter is called after an AA filter button is pressed or depressed, to change the contents of the DisplayedAA slice.
func ChangeFilter() {
	filter := make([]int, 0)
	if Filters[0] && Filters[1] { // Unowned and owned - all AA
		for i, db := range AADBs { // Ugly set of nested for loops, probably a more efficient implementation
		AllAALoop:
			for _, aas := range db {
				if !RestrictionsMet(aas[0].Restrictions, characters.CurrentChar) {
					continue AllAALoop
				}
				for _, aa := range aas {
					DisplayedAA[i] = append(DisplayedAA[i], aa)
				}
			}
		}
	} else {
		if Filters[2] && !Filters[1] { // Available - this is included in unowned AA
			for i, db := range AllAvailableAA(characters.CurrentChar) {
				DisplayedAA[i] = db
			}
		}
		if Filters[0] { // Owned
			for i, db := range characters.CurrentChar.AAs {
				for id, rank := range db {
					for j := 0; j < rank; j++ {
						DisplayedAA[i] = append(DisplayedAA[i], AADBs[i][id][j])
					}
				}
			}
		}
		if Filters[1] { // Unowned
			for i, db := range AADBs {
			UnownedAALoop:
				for id, aas := range db {
					if !RestrictionsMet(aas[0].Restrictions, characters.CurrentChar) {
						continue UnownedAALoop
					}
					rank := characters.CurrentChar.AAs[i][id]
					if rank < len(aas) {
						for j := rank; j < len(aas); j++ {
							DisplayedAA[i] = append(DisplayedAA[i], db[id][j])
						}
					}
				}
			}
		}
	}
	if !Filters[3] {
		for _, aas := range AADBs[0] {
			if aas[0].Category == "Tradeskill" {
				filter = append(filter, aas[0].ID)
			}
		}
		for _, fid := range filter {
			scratch := make([]AA, len(DisplayedAA[0]))
			copy(scratch, DisplayedAA[0])
			offset := 0
			for i, aa := range DisplayedAA[0] {
				if fid == aa.ID {
					scratch[i] = scratch[len(scratch)-1-offset]
					offset++
				}
			}
			DisplayedAA[0] = scratch[:len(scratch)-offset]
		}
	}
}
