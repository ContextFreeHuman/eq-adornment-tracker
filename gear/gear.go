/**
gear.go

Last Modified: August 18, 2019
**/

package gear

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var (
	// Slots represents the names of the slots on a character player that items can be placed into.
	Slots = []string{"LEar", "Head", "Face", "REar", "Neck", "Back", "Shoulders", "LWrist", "Feet", "Charm", "Hands", "Legs", "RWrist", "Waist", "Arms", "Chest", "LFingers", "RFingers", "Power", "Primary", "Secondary", "Range", "Ammo"}
	// SlotsO represents the names of the slots on a character player that items can be placed into, stripped of directional indicators. May be removed in the future.
	SlotsO = []string{"Ear", "Head", "Face", "Ear", "Neck", "Back", "Shoulders", "Wrist", "Feet", "Charm", "Hands", "Legs", "Wrist", "Waist", "Arms", "Chest", "Fingers", "Fingers", "Power", "Primary", "Secondary", "Range", "Ammo"}
	// ItemIndex is an index that matches an item ID with the item database it resides in.
	ItemIndex map[int]string
	// AdornDB is the database of adornment items.
	AdornDB map[int]Adornment
	// AmmoDB is the databases of items that can be placed in the ammo slot.
	AmmoDB map[int]Ammo
	// ArmorDB is the database of items that only have non-damage stats.
	ArmorDB map[int]Armor
	// BagDB is the database of items that can hold other items.
	BagDB map[int]Bag
	// ItemDB is the database of all other general items.
	ItemDB map[int]Item
	// WeaponDB is the database of all items with damage stats.
	WeaponDB map[int]Weapon
	// AllDB is a database of all items indexed by the first letter of their name and accessed by ID (e.g. 0 for "A", so AllDB[0][0] returns the ID of the first item in lexographical order)
	AllDB [][]int // TODO
	// Dummy is a "nil" object for adorn slots with no adorn currently slotted
	Dummy = Adornment{Item{0, 0, 0, 0, "", "", "", "", []string{}, []string{}, []string{}, []string{}}, Equippable{[]int{}, []int{}, []int{}, []int{}, []int{}, []int{}, []string{}}, 0}

	dbs     = []string{"adorn", "ammo", "armor", "bag", "item", "weapon"}
	vitals  = []string{"AC", "HP", "Mana", "END", "Haste%", "Purity"}
	stats   = []string{"STR", "STA", "INT", "WIS", "AGI", "DEX", "CHA"}
	resists = []string{"Magic", "Fire", "Cold", "Disease", "Poison", "Corruption"}
	other   = []string{"Attack", "HP Regen", "Mana Regen", "Heal Amount", "Spell Damage", "Clairvoyance", "Luck"}
	damage  = []string{"Base Damage", "Magic Damage", "Cold Damage", "Backstab Damage", "Delay", "Bonus Damage", "Range"}
)

// Ear, Head, Face, Ear, Neck, Back, Shoulder, Wrist, Feet, Charm, Hands, Legs, Wrist, Waist, Arms, Chest, Finger, Finger, Power, Primary, Secondary, Range, Ammo

// Item represents an item with the most basic information available.
type Item struct {
	ID, Reqlevel, Reclevel int
	Weight                 float32
	Name, Size, Lore, Info string
	// Item Information: \n
	Attributes, Classes, Races, Effects []string
}

// Equippable represents an item that can be equipped by a player character.
type Equippable struct {
	Vitals, Resists, Stats, Hstats, Other, SlotTypes []int
	// Slots: 1 (General: Single Stat), 2 (General: Multiple Stat), 3 (General: Spell Effect)
	// 4 (Weapon: General), 5 (General: Multiple Stat), 6 (Weapon: Base Damage)
	// 7 (General: Group), 8 (General: Raid), 9 (General: Point Augments)
	// 10 (Crafted: Common), 11 (Crafted: Group), 12 (Crafted: Raid), 13 (Energeiac: Group)
	// 20 (Weapon Ornamentation), 21 (Armor Ornamentation)
	Slots []string
}

// Adornment represents an adornment item that can be slotted into certain regular items.
type Adornment struct {
	Info      Item
	Stats     Equippable
	Distiller int
	// This Augmentation fits in slot types: _, _
}

// Armor represents an armor item that can be equipped by player characters.
type Armor struct {
	Info  Item
	Stats Equippable
}

// Weapon represents an armor item that can be equipped by player characters.
type Weapon struct {
	Info   Item
	Stats  Equippable
	Skill  string
	Damage []int
}

// Ammo represents an ammo item that can be used by player characters.
type Ammo struct {
	Info   Item
	Skill  string
	Damage []int
}

// Bag represents a bag item that can be carried by player characters.
type Bag struct {
	Info                      Item
	sizeCap                   string
	capacity, weightReduction int
}

// Equipped represents one of the armor or weapon slots on a character player.
type Equipped struct {
	Status  bool   // If an item is slotted
	EquipW  Weapon // Two or all of these will be null, there are specific cases for accessing these fields
	EquipA  Armor
	EquipAm Ammo
	Adorns  []Adornment
}

// LoadIndex reads the itemIndex file and loads the index into memory.
func LoadIndex() {
	ItemIndex = make(map[int]string, 0)
	index, err := os.Open("gear/itemIndex")
	if err != nil {
		fmt.Println("Error: Failed to open itemIndex file.")
		return
	}
	defer index.Close()
	stepper := bufio.NewScanner(index)
	for stepper.Scan() {
		line := strings.Fields(stepper.Text())
		id, err := strconv.Atoi(line[0])
		if err != nil {
			fmt.Printf("Error: Failed to convert %v to an integer.\n", line[0])
			continue
		}
		ItemIndex[id] = line[1]
	}
}

// loadItem reads and collects the basic info about an item.
func loadItem(line []string) (i, id int, it Item) {
	i = 4
	id, err := strconv.Atoi(line[0])
	if err != nil {
		fmt.Printf("Error: Failed to convert %v to an integer.\n", line[0])
		return
	}
	weight64, err := strconv.ParseFloat(line[1], 32)
	if err != nil {
		fmt.Printf("Error: Failed to convert %v to a float.\n", line[1])
		return
	}
	weight := float32(weight64)
	reqLevel, err := strconv.Atoi(line[2])
	if err != nil {
		fmt.Printf("Error: Failed to convert %v to an integer.\n", line[2])
		return
	}
	recLevel, err := strconv.Atoi(line[3])
	if err != nil {
		fmt.Printf("Error: Failed to convert %v to an integer.\n", line[3])
		return
	}
	strs := make([]string, 4)
	for j := 0; j < 4; j++ {
		var phrase string
		for line[i] != ";" {
			phrase += line[i]
			if line[i+1] != ";" {
				phrase += " "
			}
			i++
		}
		strs[j] = phrase
		i++
	}
	stras := make([][]string, 3)
	for j := 0; j < 3; j++ {
		stras[j] = make([]string, 0)
		i++
		for line[i] != "]" {
			stras[j] = append(stras[j], line[i])
			i++
		}
		i++
	}
	effects := make([]string, 0)
	i++
	var effect string
	for line[i] != "]" {
		if line[i] == ";" {
			effects = append(effects, effect)
			effect = ""
			i++
			continue
		}
		effect += line[i] + " "
		i++
	}
	if effect != "" {
		effects = append(effects, effect)
	}
	i++
	it = Item{id, reqLevel, recLevel, weight, strs[0], strs[1], strs[2], strs[3], stras[0], stras[1], stras[2], effects}
	return
}

func loadStats(line []string, index int) (i int, it Equippable) {
	i = index
	stats := make([][]int, 6)
	for j := 0; j < 6; j++ {
		stats[j] = make([]int, 0)
		i++
		for line[i] != "]" {
			n, err := strconv.Atoi(line[i])
			if err != nil {
				fmt.Printf("Error: Failed to convert %v to an integer.\n", line[i])
				fmt.Println(line)
				break
			}
			stats[j] = append(stats[j], n)
			i++
		}
		if line[i] == "]" {
			i++
		}
	}
	slots := make([]string, 0)
	i++
	for line[i] != "]" {
		slots = append(slots, line[i])
		i++
	}
	i++
	it = Equippable{stats[0], stats[1], stats[2], stats[3], stats[4], stats[5], slots}
	return
}

// LoadItems reads the database files for all the game items and populates the databases into memory.
func LoadItems() {
	AllDB = make([][]int, 43)
	for i := 0; i < 43; i++ {
		AllDB[i] = make([]int, 0) // Going to be some extra
	}
	for i := range dbs {
		switch i {
		case 0:
			db, err := os.Open("gear/adornDB")
			if err != nil {
				fmt.Println("Error: Failed to open adornment database.")
				continue
			}
			defer db.Close()
			AdornDB = make(map[int]Adornment, 0)
			stepper := bufio.NewScanner(db)
			for stepper.Scan() {
				line := strings.Fields(stepper.Text())
				i, id, info := loadItem(line)
				j, stats := loadStats(line, i)
				distiller, err := strconv.Atoi(line[j])
				if err != nil {
					fmt.Printf("Error: Failed to convert %v to an integer.\n", line[j])
					break
				}
				AdornDB[id] = Adornment{info, stats, distiller}
				AllDB[int(info.Name[0])-48] = append(AllDB[int(info.Name[0])-48], id) // ASCII value reduced to 0
			}
		case 1:
			db, err := os.Open("gear/ammoDB")
			if err != nil {
				fmt.Println("Error: Failed to open item database.")
				continue
			}
			defer db.Close()
			AmmoDB = make(map[int]Ammo, 0)
			stepper := bufio.NewScanner(db)
			for stepper.Scan() {
				line := strings.Fields(stepper.Text())
				i, id, it := loadItem(line)
				info := it
				skill := line[i]
				dmg := make([]int, 7)
				i += 2
				j := 0
				for line[i] != "]" {
					n, err := strconv.Atoi(line[i])
					if err != nil {
						fmt.Printf("Error: Failed to convert (ammo) %v to an integer.\n", line[i])
						break
					}
					dmg[j] = n
					i++
					j++
				}
				AmmoDB[id] = Ammo{info, skill, dmg}
				AllDB[int(info.Name[0])-48] = append(AllDB[int(info.Name[0])-48], id)
			}
		case 2:
			db, err := os.Open("gear/armorDB")
			if err != nil {
				fmt.Println("Error: Failed to open armor database.")
				continue
			}
			defer db.Close()
			ArmorDB = make(map[int]Armor, 0)
			stepper := bufio.NewScanner(db)
			for stepper.Scan() {
				line := strings.Fields(stepper.Text())
				i, id, it := loadItem(line)
				info := it
				_, stats := loadStats(line, i)
				ArmorDB[id] = Armor{info, stats}
				AllDB[int(info.Name[0])-48] = append(AllDB[int(info.Name[0])-48], id)
			}
		case 3:
			db, err := os.Open("gear/bagDB")
			if err != nil {
				fmt.Println("Error: Failed to open bag database.")
				continue
			}
			defer db.Close()
			BagDB = make(map[int]Bag, 0)
			stepper := bufio.NewScanner(db)
			for stepper.Scan() {
				line := strings.Fields(stepper.Text())
				i, id, info := loadItem(line)
				cap, err := strconv.Atoi(line[i+1])
				if err != nil {
					fmt.Printf("Error: Failed to convert %v to an integer.\n", cap)
					break
				}
				red, err := strconv.Atoi(line[i+2])
				if err != nil {
					fmt.Printf("Error: Failed to convert %v to an integer.\n", red)
					break
				}
				BagDB[id] = Bag{info, line[i], cap, red}
				AllDB[int(info.Name[0])-48] = append(AllDB[int(info.Name[0])-48], id)
			}
		case 4:
			db, err := os.Open("gear/itemDB")
			if err != nil {
				fmt.Println("Error: Failed to open item database.")
				continue
			}
			defer db.Close()
			ItemDB = make(map[int]Item, 0)
			stepper := bufio.NewScanner(db)
			for stepper.Scan() {
				_, id, it := loadItem(strings.Fields(stepper.Text()))
				ItemDB[id] = it
				AllDB[int(it.Name[0])-48] = append(AllDB[int(it.Name[0])-48], id)
			}
		case 5:
			db, err := os.Open("gear/weaponDB")
			if err != nil {
				fmt.Println("Error: Failed to open weapon database.")
				continue
			}
			defer db.Close()
			WeaponDB = make(map[int]Weapon, 0)
			stepper := bufio.NewScanner(db)
			for stepper.Scan() {
				line := strings.Fields(stepper.Text())
				i, id, info := loadItem(line)
				j, stats := loadStats(line, i)
				skill := line[j]
				dmg := make([]int, 7)
				for line[j] != "[" {
					skill += line[j] + " " // Space will probably need to be removed later
					j++
				}
				j++
				k := 0
				for line[j] != "]" {
					n, err := strconv.Atoi(line[j])
					if err != nil {
						fmt.Printf("Error: Failed to convert (weapon) %v to an integer.\n", line[j])
						break
					}
					dmg[k] = n
					j++
					k++
				}
				WeaponDB[id] = Weapon{info, stats, skill, dmg}
				AllDB[int(info.Name[0])-48] = append(AllDB[int(info.Name[0])-48], id)
			}
		}
	}
}

func printEffects(it Item) (str string) {
	for _, effect := range it.Effects {
		var name, details string
		var i int
		examine := strings.Fields(effect)
		for ; examine[i] != ":"; i++ {
			name += examine[i] + " "
		}
		i++
		for ; i < len(examine); i++ {
			details += examine[i] + " "
		}
		str += name + ": \n" + details + "\n\n"
	}
	return
}

func (item *Item) print() (str string) {
	str += item.Name + "\n"
	for _, attribute := range item.Attributes {
		if attribute == "NoTrade" {
			str += "No Trade "
		} else {
			str += attribute + " "
		}
	}
	if len(item.Attributes) != 0 {
		str += "\n\n"
	} else {
		str += "\n"
	}
	if item.Reclevel != 0 {
		str += "Rec Level: " + strconv.FormatInt(int64(item.Reclevel), 10) + "    "
	}
	if item.Reqlevel != 0 {
		str += "Req Level: " + strconv.FormatInt(int64(item.Reqlevel), 10) + "\n"
	} else {
		str += "\n"
	}
	str += "Size: " + item.Size + "    "
	if item.Weight != 0 {
		str += "Weight: " + strconv.FormatFloat(float64(item.Weight), 'f', -1, 32) + "\n\n"
	} else {
		str += "\n\n"
	}
	if item.Info != "" {
		str += item.Info + "\n\n"
	}
	if item.Lore != "" {
		str += item.Lore + "\n"
	}
	return
}

func (item *Equippable) print() (str string) {
	nlFlag := false
	aVitals := item.Vitals
	if len(aVitals) == 0 {
		aVitals = []int{0, 0, 0, 0, 0, 0}
	}
	aResists := item.Resists
	if len(aResists) == 0 {
		aResists = []int{0, 0, 0, 0, 0, 0}
	}
	aStats := item.Stats
	if len(aStats) == 0 {
		aStats = []int{0, 0, 0, 0, 0, 0, 0}
	}
	aHStats := item.Hstats
	if len(aHStats) == 0 {
		aHStats = []int{0, 0, 0, 0, 0, 0, 0}
	}
	aOther := item.Other
	if len(aOther) == 0 {
		aOther = []int{0, 0, 0, 0, 0, 0, 0}
	}
	str += "\n"
	for i := 0; i < 6; i++ {
		if aVitals[i] != 0 {
			nlFlag = true
			str += vitals[i] + ": " + strconv.FormatInt(int64(aVitals[i]), 10) + "\n"
		}
	}
	if nlFlag {
		str += "\n"
		nlFlag = false
	}
	for i := 0; i < 7; i++ {
		if aStats[i] != 0 {
			str += stats[i] + ": " + strconv.FormatInt(int64(aStats[i]), 10)
		}
		if aHStats[i] != 0 {
			if aStats[i] == 0 {
				str += stats[i] + ": " + strconv.FormatInt(int64(0), 10)
			}
			str += "+" + strconv.FormatInt(int64(aHStats[i]), 10)
		}
		if aStats[i] != 0 || aHStats[i] != 0 {
			nlFlag = true
			str += "\n"
		}
	}
	if nlFlag {
		str += "\n"
		nlFlag = false
	}
	for i := 0; i < 6; i++ {
		if aResists[i] != 0 {
			nlFlag = true
			str += resists[i] + ": " + strconv.FormatInt(int64(aResists[i]), 10) + "\n"
		}
	}
	if nlFlag {
		str += "\n"
		nlFlag = false
	}
	for i := 0; i < 7; i++ {
		if aOther[i] != 0 {
			nlFlag = true
			str += other[i] + ": " + strconv.FormatInt(int64(aOther[i]), 10) + "\n"
		}
	}
	if nlFlag {
		str += "\n"
		nlFlag = false
	}
	return
}

// Print produces a string describing the weapon (meant for display in a TextView)
func (item *Weapon) Print() (str string) {
	str = item.Info.print() + item.Stats.print()
	for i, dmg := range damage {
		if item.Damage[i] != 0 {
			str += dmg + ": " + strconv.FormatInt(int64(item.Damage[i]), 10) + "\n"
		}
	}
	if len(item.Info.Effects) > 0 {
		str += printEffects(item.Info)
	}
	return
}

// Print produces a string describing the armor (meant for display in a TextView)
func (item *Armor) Print() (str string) {
	str = item.Info.print() + item.Stats.print()
	if len(item.Info.Effects) > 0 {
		str += printEffects(item.Info)
	}
	return
}

// Print produces a string describing the ammo (meant for display in a TextView)
func (item *Ammo) Print() (str string) {
	str = item.Info.print()
	for i, dmg := range damage {
		if item.Damage[i] != 0 {
			str += dmg + ": " + strconv.FormatInt(int64(item.Damage[i]), 10) + "\n"
		}
	}
	return
}

// Print produces a string describing the adornment (meant for display in a TextView)
func (item *Adornment) Print() (str string) {
	str = item.Info.print() + item.Stats.print() + "You must use a solvent Class "
	str += strconv.FormatInt(int64(item.Distiller), 10) + " Augmentation Distiller or higher, or use a Perfected Augmentation Distiller, to remove this augment safely.\n"
	if len(item.Info.Effects) > 0 {
		str += "\n" + printEffects(item.Info)
	} else {
		str += "\n"
	}
	return // TODO: Roman Numerals
}
