/**
main.go

Last Modified: August 6, 2019
**/

package main

import (
	"fmt"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/ContextFreeHuman/eq-progression-tracker/aa"
	"gitlab.com/ContextFreeHuman/eq-progression-tracker/gear"
	"gitlab.com/ContextFreeHuman/eq-progression-tracker/ts"
	"gitlab.com/ContextFreeHuman/eq-progression-tracker/ui"
)

func main() {
	// Data Initialization
	aa.ImportDefault()
	aa.LoadIndex()
	gear.LoadItems()
	gear.LoadIndex()
	ts.ImportDefault()

	// GUI Initialization
	gtk.Init(nil)

	builder, err := gtk.BuilderNewFromFile("ui/ui.glade")
	if err != nil {
		fmt.Println("Error: Failed to initialize builder, terminating program.")
		return
	}

	ui.InitUI(builder)

	gtk.Main()
}
