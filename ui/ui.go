/**
ui.go

Last Modified: January 15, 2020
**/

package ui

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"github.com/gotk3/gotk3/gtk"
	"gitlab.com/ContextFreeHuman/eq-progression-tracker/aa"
	"gitlab.com/ContextFreeHuman/eq-progression-tracker/characters"
	"gitlab.com/ContextFreeHuman/eq-progression-tracker/gear"
	"gitlab.com/ContextFreeHuman/eq-progression-tracker/ts"
)

var (
	builder           *gtk.Builder
	startWindow       *gtk.Window
	creationWindow    *gtk.Window
	selectionWindow   *gtk.FileChooserDialog
	mainWindow        *gtk.Window
	itemSelectWindow  *gtk.Window
	adornSelectWindow *gtk.Window

	itemSelectList *gtk.ListStore

	nameToID map[string]int

	allNames []string

	listStores []*gtk.ListStore

	curID   int
	curRank int
	curSlot = -1
	selID   = 0
	selSlot = -1

	changeAdorn bool
)

// InitUI initializes the global builder variable as well as the global window variables, sets up clickable objects in the interface, and then shows the starting window.
func InitUI(b *gtk.Builder) {
	builder = b // Initialize global builder variable

	initWindows() // Initialize windows

	// Initialize events
	loadStart()
	loadCreation()
	loadSelection()
	loadMain()
	loadItemSelect()
	loadAdornSelect()

	// Display starting window
	startWindow.ShowAll()
}

func initWindows() {
	startWindow = loadWindow("Start Window")
	startWindow.Connect("destroy", func() { // If window is closed, program ends
		gtk.MainQuit()
	})

	creationWindow = loadWindow("Creation Window")
	creationWindow.Connect("destroy", func() { // This can only be done through taskbar or task manager
		gtk.MainQuit()
	})

	selectionWindow = loadFileChooserDialog("Selection Window")
	selectionWindow.Connect("destroy", func() {
		gtk.MainQuit()
	})

	mainWindow = loadWindow("Main Window")
	mainWindow.Connect("destroy", func() {
		gtk.MainQuit()
	})

	itemSelectWindow = loadWindow("ItemSelectWindow") // Do not terminate if window close

	adornSelectWindow = loadWindow("AdornSelectWindow") // Do not terminate if window close
}

// loadStart connects the buttons in the starting window to their respective functions.
func loadStart() {
	// Make buttons functional
	createButton := loadButton("Create Character")
	createButton.Connect("clicked", func() {
		creationWindow.ShowAll()
		startWindow.Hide()
	})
	selectButton := loadButton("Select Character")
	selectButton.Connect("clicked", func() {
		selectionWindow.ShowAll()
		startWindow.Hide()
	})
}

// loadCreation connects the buttons in the character creation window to their respective functions.
func loadCreation() {
	// Make buttons functional
	cancelButton := loadButton("Cancel Creation")
	cancelButton.Connect("clicked", func() {
		startWindow.ShowAll()
		creationWindow.Hide()
	})
	confirmButton := loadButton("Confirm Creation")
	confirmButton.Connect("clicked", func() {
		inputs := []string{"Name", "Level", "Server", "Class", "Race", "Deity"}
		entries, combos := make([]*gtk.Entry, 3), make([]*gtk.ComboBoxText, 3)
		for i, input := range inputs {
			if i < 3 {
				entries[i] = loadEntry("Char" + input)
			} else {
				combos[i-3] = loadComboBoxText("Char" + input)
			}
		}
		inputs = make([]string, 5)
		var n string
		i := 0
		for _, entry := range entries {
			input := getEntry(entry)
			if input == "" {
				fmt.Println("Error: User provided no input")
				return
			}
			if i == 1 && n == "" {
				n = input
				i--
			} else {
				inputs[i] = input
			}
			i++
		}
		for _, combo := range combos {
			inputs[i] = combo.GetActiveText()
			i++
		}
		level, err := strconv.Atoi(n)
		if err != nil {
			fmt.Printf("Error: Failed to convert %v to an integer", n)
			return
		}
		if level < 1 || level > 110 {
			return // TODO: Explain to user
		}
		status := characters.CreateCharacter(inputs[0], inputs[2], inputs[3], inputs[4], inputs[1], level, aa.AAIndex)
		if status == "" {

			// Display character information on first tab
			gearText := loadTextView("CharText")
			charBuffer, err := gearText.GetBuffer()
			if err != nil {
				fmt.Println("Error: Failed to load buffer for first tab TextView. Terminating program.")
				panic(err)
			}
			charBuffer.SetText(characters.CurrentChar.Name + " on " + characters.CurrentChar.Server + "\n" + strconv.FormatInt(int64(characters.CurrentChar.Level), 10) + " " + characters.CurrentChar.Race + " " + characters.CurrentChar.Class + "\n" + characters.CurrentChar.Deity)

			// Initialize whether buttons should be clickable
			levelButton := loadButton("LevelButton")
			if characters.CurrentChar.Level == 110 {
				levelButton.SetSensitive(false)
			}
			delevelButton := loadButton("DelevelButton")
			if characters.CurrentChar.Level == 1 {
				delevelButton.SetSensitive(false)
			}

			aa.ResetDisplayed()
			aa.ChangeFilter() // Populate lists with all AA
			populateLists(listStores)

			creationWindow.Hide()
			mainWindow.ShowAll()
		}
	})
}

// loadSelection connects the buttons in the file chooser to their respective functions.
func loadSelection() {
	var path string
	exe, err := os.Executable()
	if err != nil {
		fmt.Println("Error: Failed to get path to executable.")
		exe = ""
		err = nil // Continue
	}
	if exe != "" {
		path = filepath.Dir(exe)
	}
	if exe != "nil" {
		selectionWindow.SetCurrentFolder(path + "/characters")
	}

	cancelButton := loadButton("Cancel Selection")
	cancelButton.Connect("clicked", func() {
		selectionWindow.Hide()
		startWindow.Show()
	})
	selectButton := loadButton("Select File")
	selectButton.Connect("clicked", func() {
		folder := selectionWindow.GetFilename()
		status := characters.LoadCharacter(folder, aa.AAIndex)
		if status == "" { // TODO: Produce a dialog explaining why a failure occurred
			selectionWindow.Hide()

			// Display character information on first tab
			gearText := loadTextView("CharText")
			charBuffer, err := gearText.GetBuffer()
			if err != nil {
				fmt.Println("Error: Failed to load buffer for first tab TextView. Terminating program.")
				panic(err)
			}
			charBuffer.SetText(characters.CurrentChar.Name + " on " + characters.CurrentChar.Server + "\n" + strconv.FormatInt(int64(characters.CurrentChar.Level), 10) + " " + characters.CurrentChar.Race + " " + characters.CurrentChar.Class + "\n" + characters.CurrentChar.Deity)

			// Initialize whether buttons should be clickable
			levelButton := loadButton("LevelButton")
			if characters.CurrentChar.Level == 110 {
				levelButton.SetSensitive(false)
			}
			delevelButton := loadButton("DelevelButton")
			if characters.CurrentChar.Level == 1 {
				delevelButton.SetSensitive(false)
			}

			aa.ResetDisplayed()
			aa.ChangeFilter() // Populate lists with all AA
			populateLists(listStores)

			mainWindow.ShowAll()
		}
	})
}

// loadMain creates the main window and connects all clickable objects to their respective functions.
func loadMain() {
	// Initialize the ListStores
	listStores = make([]*gtk.ListStore, 6)
	for i, db := range aa.DBs {
		listStores[i] = loadListStore(strings.ToUpper(db[0:1]) + db[1:len(db)] + "AA")
	}

	tsListStores := make([]*gtk.ListStore, 12)
	for i, db := range ts.DBs {
		tsListStores[i] = loadListStore(strings.ToUpper(db[0:1]) + db[1:len(db)] + "Recipes")
	}
	populateRecipeLists(tsListStores)

	// Define the action for the menu item "Refresh Databases"
	refresh := loadMenuItem("Refresh Databases")
	refresh.Connect("activate", func() {
		aa.ImportDefault()
		aa.LoadIndex()
		gear.LoadItems()
		gear.LoadIndex()
		ts.ImportDefault()

		clearLists(listStores)
		clearLists(tsListStores)
		aa.ResetDisplayed()
		aa.ChangeFilter()
		populateLists(listStores)
		populateRecipeLists(tsListStores)
	})

	// Initialize TextViews
	charText := loadTextView("CharText")
	aaText := loadTextView("AAText")
	tradeskillText := loadTextView("TradeskillText")
	charBuffer, err := charText.GetBuffer()
	if err != nil {
		fmt.Println("Error: Failed to load buffer for character info TextView. Terminating program.")
		panic(err)
	}
	gearText := loadTextView("GearText")
	gearBuffer, err := gearText.GetBuffer()
	if err != nil {
		fmt.Println("Error: Failed to load buffer for gear info TextView. Terminating program.")
		panic(err)
	}
	adornText := loadTextView("AdornText")
	adornBuffer, err := adornText.GetBuffer()
	if err != nil {
		fmt.Println("Error: Failed to load buffer for adornment info TextView. Terminating program.")
		panic(err)
	}

	// Initialize leveling Buttons
	levelButton := loadButton("LevelButton")
	delevelButton := loadButton("DelevelButton")
	levelButton.Connect("clicked", func() {
		characters.CurrentChar.Level++
		characters.WriteCharInfo()
		charBuffer.SetText(characters.CurrentChar.Name + " on " + characters.CurrentChar.Server + "\n" + strconv.FormatInt(int64(characters.CurrentChar.Level), 10) + " " + characters.CurrentChar.Race + " " + characters.CurrentChar.Class + "\n" + characters.CurrentChar.Deity)
		if characters.CurrentChar.Level == 110 {
			levelButton.SetSensitive(false)
		} else {
			levelButton.SetSensitive(true)
		}
		delevelButton.SetSensitive(true)

		clearLists(listStores) // Change available AA displays
		aa.ResetDisplayed()
		aa.ChangeFilter()
		populateLists(listStores)
	})
	delevelButton.Connect("clicked", func() {
		characters.CurrentChar.Level--
		characters.WriteCharInfo()
		charBuffer.SetText(characters.CurrentChar.Name + " on " + characters.CurrentChar.Server + "\n" + strconv.FormatInt(int64(characters.CurrentChar.Level), 10) + " " + characters.CurrentChar.Race + " " + characters.CurrentChar.Class + "\n" + characters.CurrentChar.Deity)
		if characters.CurrentChar.Level == 1 {
			delevelButton.SetSensitive(false)
		} else {
			delevelButton.SetSensitive(true)
		}
		levelButton.SetSensitive(true)

		clearLists(listStores) // Change available AA displays
		aa.ResetDisplayed()
		aa.ChangeFilter()
		populateLists(listStores)
	})

	// Initialize gear slot buttons
	gearButtons := make([]*gtk.Button, 23)
	for i, slot := range gear.Slots {
		button := loadButton(slot + "Button")
		j := i
		if i < 19 {
			button.Connect("clicked", func() {
				curSlot = j
				if !characters.CurrentChar.Gear[j].Status {
					gearBuffer.SetText("No gear in slot.")
					adornBuffer.SetText("")
				} else {
					item := characters.CurrentChar.Gear[j].EquipA
					gearBuffer.SetText(item.Print())
					adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[j], item.Stats))
				}
			})
		} else {
			if i < 22 {
				button.Connect("clicked", func() {
					curSlot = j
					if !characters.CurrentChar.Gear[j].Status {
						gearBuffer.SetText("No gear in slot.")
						adornBuffer.SetText("")
					} else {
						if characters.CurrentChar.Gear[j].EquipW.Info.ID != -1 {
							item := characters.CurrentChar.Gear[j].EquipW
							gearBuffer.SetText(item.Print())
							adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[j], item.Stats))
						} else {
							item := characters.CurrentChar.Gear[j].EquipA
							gearBuffer.SetText(item.Print())
							adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[j], item.Stats))
						}
					}
				})
			} else {
				button.Connect("clicked", func() {
					curSlot = j
					if !characters.CurrentChar.Gear[j].Status {
						gearBuffer.SetText("No gear in slot.")
						adornBuffer.SetText("")
					} else {
						item := characters.CurrentChar.Gear[j].EquipAm
						gearBuffer.SetText(item.Print())
						adornBuffer.SetText("")
					}
				})
			}
		}
		gearButtons[i] = button
	}

	// Initialize gear change buttons
	changeGearButton := loadButton("ChangeGearButton")
	changeGearButton.Connect("clicked", func() {
		if curSlot != -1 {
			changeAdorn = false
			getApplicableItems()
			itemSelectWindow.ShowAll()
		}
	})
	changeAdornsButton := loadButton("ChangeAdornsButton")
	changeAdornsButton.Connect("clicked", func() {
		if curSlot != -1 && curSlot != 22 && characters.CurrentChar.Gear[curSlot].Status { // Unselected, empty slot and ammo slot should not have adornments
			changeAdorn = true
			adornCombo := loadComboBoxText("AdornCombo")
			adornCombo.RemoveAll()
			if curSlot < 19 {
				for _, slot := range characters.CurrentChar.Gear[curSlot].EquipA.Stats.SlotTypes {
					adornCombo.Append(characters.CurrentChar.Gear[curSlot].EquipA.Info.Name, strconv.FormatInt(int64(slot), 10))
				}
			} else {
				if characters.CurrentChar.Gear[curSlot].EquipA.Info.ID != -1 {
					for _, slot := range characters.CurrentChar.Gear[curSlot].EquipA.Stats.SlotTypes {
						adornCombo.Append(characters.CurrentChar.Gear[curSlot].EquipA.Info.Name, strconv.FormatInt(int64(slot), 10))
					}
				} else {
					for _, slot := range characters.CurrentChar.Gear[curSlot].EquipW.Stats.SlotTypes {
						adornCombo.Append(characters.CurrentChar.Gear[curSlot].EquipW.Info.Name, strconv.FormatInt(int64(slot), 10))
					}
				}
			}
			adornSelectWindow.ShowAll()
		}
	})

	// Initialize AA filter buttons
	ownedFilterToggle := loadToggleButton("OwnedFilterToggle")
	ownedFilterToggle.Connect("toggled", func() {
		clearLists(listStores)
		aa.ResetDisplayed()
		if ownedFilterToggle.GetActive() {
			aa.Filters[0] = true
		} else {
			aa.Filters[0] = false
		}
		aa.ChangeFilter()
		populateLists(listStores)
	})
	unownedFilterToggle := loadToggleButton("UnownedFilterToggle")
	unownedFilterToggle.Connect("toggled", func() {
		clearLists(listStores)
		aa.ResetDisplayed()
		if unownedFilterToggle.GetActive() {
			aa.Filters[1] = true
		} else {
			aa.Filters[1] = false
		}
		aa.ChangeFilter()
		populateLists(listStores)
	})
	availableFilterToggle := loadToggleButton("AvailableFilterToggle")
	availableFilterToggle.Connect("toggled", func() {
		clearLists(listStores)
		aa.ResetDisplayed()
		if availableFilterToggle.GetActive() {
			aa.Filters[2] = true
		} else {
			aa.Filters[2] = false
		}
		aa.ChangeFilter()
		populateLists(listStores)
	})
	tsFilterToggle := loadToggleButton("TSFilterToggle")
	tsFilterToggle.Connect("toggled", func() {
		clearLists(listStores)
		aa.ResetDisplayed()
		if tsFilterToggle.GetActive() {
			aa.Filters[3] = true
		} else {
			aa.Filters[3] = false
		}
		aa.ChangeFilter()
		populateLists(listStores)
	})

	// Initialize AA ID entry and button for adding and removing AA from characters
	addAAButton := loadButton("AddAAButton")
	addAAButton.Connect("clicked", func() {
		db := aa.AAIndex[curID]
		_, ok := characters.CurrentChar.AAs[db][curID]
		if !ok {
			characters.CurrentChar.AAs[db][curID] = 1
		} else {
			characters.CurrentChar.AAs[db][curID]++
		}
		characters.WriteCharAA()
		clearLists(listStores)
		aa.ResetDisplayed()
		aa.ChangeFilter()
		populateLists(listStores)
	})
	removeAAButton := loadButton("RemoveAAButton")
	removeAAButton.Connect("clicked", func() {
		db := aa.AAIndex[curID]
		rank := characters.CurrentChar.AAs[db][curID]
		if rank > 1 {
			characters.CurrentChar.AAs[db][curID]--
		} else {
			delete(characters.CurrentChar.AAs[db], curID)
		}
		characters.WriteCharAA()
		clearLists(listStores)
		aa.ResetDisplayed()
		aa.ChangeFilter()
		populateLists(listStores)
	})

	// Initialize the TreeViews
	aaTreeViews := make([]*gtk.TreeView, 6)
	for i, db := range aa.DBs {
		aaTreeView := loadTreeView(strings.ToUpper(db[:1]) + db[1:len(db)] + "AAView")
		aaTreeView.Connect("cursor-changed", func() {
			aaBuffer, err := aaText.GetBuffer()
			if err != nil {
				fmt.Println("Error: Failed to get TextBuffer from TextView.")
				return
			}

			selection, err := aaTreeView.GetSelection()
			if err != nil {
				fmt.Println("Error: Failed to get current selection.")
				return
			}
			model, iter, ok := selection.GetSelected()
			if !ok { // Occurs when adding and removing AA - no error message (currently)
				aaBuffer.SetText("")
				return
			}
			treeModel := model.(*gtk.TreeModel)
			vid, err := treeModel.GetValue(iter, 0) // Get ID
			if err != nil {
				fmt.Println("Error: Failed to get ID value from treeModel.")
				return
			}
			uid, err := vid.GoValue()
			if err != nil {
				fmt.Println("Error: Failed to convert ID value to int.")
				return
			}
			id := uid.(int)                           // Assert type
			vrank, err := treeModel.GetValue(iter, 2) // Get rank
			if err != nil {
				fmt.Println("Error: Failed to get rank value from TreeModel.")
				return
			}
			urank, err := vrank.GoValue()
			if err != nil {
				fmt.Println("Error: Failed to convert rank value to int.")
				return
			}
			rank := urank.(int)

			selectedAA := aa.AADBs[aa.AAIndex[id]][id][rank-1] // Get struct for selected AA
			var notes, timerText string
			if selectedAA.Notes != "" {
				notes = "\nNotes: " + selectedAA.Notes
			}
			if selectedAA.Timer != -1 {
				timerText = "\nTimer: " + strconv.FormatInt(int64(selectedAA.Timer), 10) + " Cooldown: " + strconv.FormatInt(int64(selectedAA.Cooldown), 10)
			}
			aaBuffer.SetText("Description: " + selectedAA.Description + notes + timerText)

			availableAA := aa.AvailableAA(aa.AADBs[aa.AAIndex[id]], characters.CurrentChar.AAs[aa.AAIndex[id]], characters.CurrentChar) // Gray out invalid button(s)
			for i, available := range availableAA {
				if id == available.ID {
					crank, _ := characters.CurrentChar.AAs[aa.AAIndex[id]][id] // Defaults to 0 if no entry
					if crank+1 == rank {
						addAAButton.SetSensitive(true)
					} else {
						addAAButton.SetSensitive(false)
					}
					break
				}
				if i == len(availableAA)-1 {
					addAAButton.SetSensitive(false)
				}
			}

			if addAAButton.IsSensitive() { // If an AA is available, the character doesn't have it
				removeAAButton.SetSensitive(false)
			} else {
				crank, ok := characters.CurrentChar.AAs[aa.AAIndex[id]][id]
				if ok {
					if rank == crank {
						removeAAButton.SetSensitive(true)
					} else {
						removeAAButton.SetSensitive(false)
					}
				} else {
					removeAAButton.SetSensitive(false)
				}
			}
			curID = id
			curRank = rank
		})
		aaTreeViews[i] = aaTreeView
	}

	recipeTreeViews := make([]*gtk.TreeView, 12)
	for i, db := range ts.DBs {
		recipeTreeView := loadTreeView(strings.ToUpper(db[:1]) + db[1:len(db)] + "View")

		recipeTreeView.Connect("cursor-changed", func() {
			tradeskillBuffer, err := tradeskillText.GetBuffer()
			if err != nil {
				fmt.Println("Error: Failed to get TextBuffer from TextView.")
				return
			}

			selection, err := recipeTreeView.GetSelection()
			if err != nil {
				fmt.Println("Error: Failed to get current selection.")
				return
			}
			model, iter, ok := selection.GetSelected()
			if !ok {
				//tradeskillBuffer.SetText("")
				return
			}
			treeModel := model.(*gtk.TreeModel)
			vid, err := treeModel.GetValue(iter, 0) // Get ID
			if err != nil {
				fmt.Println("Error: Failed to get ID value from treeModel.")
				return
			}
			uid, err := vid.GoValue()
			if err != nil {
				fmt.Println("Error: Failed to convert ID value to int.")
				return
			}
			id := uid.(int) // Assert type

			tradeskillBuffer.SetText("WIP - ID: " + strconv.FormatInt(int64(id), 10))
		})

		recipeTreeViews[i] = recipeTreeView
	}
}

// loadItemSelect connects the buttons in the item selection window to their respective functions.
func loadItemSelect() {
	var adornIndex int
	if changeAdorn {
		if curSlot < 19 {
			for i, slot := range characters.CurrentChar.Gear[curSlot].EquipA.Stats.SlotTypes {
				if slot == selSlot {
					adornIndex = i
					break
				}
			}
		} else {
			if characters.CurrentChar.Gear[curSlot].EquipA.Info.ID != -1 {
				for i, slot := range characters.CurrentChar.Gear[curSlot].EquipA.Stats.SlotTypes {
					if slot == selSlot {
						adornIndex = i
						break
					}
				}
			} else {
				for i, slot := range characters.CurrentChar.Gear[curSlot].EquipW.Stats.SlotTypes {
					if slot == selSlot {
						adornIndex = i
						break
					}
				}
			}
		}
	}

	// Get TextView and buffer for use if an item is selected
	gearText := loadTextView("GearText")
	gearBuffer, err := gearText.GetBuffer()
	if err != nil {
		fmt.Println("Error: Failed to load buffer for gear info TextView. Terminating program.")
		panic(err)
	}
	adornText := loadTextView("AdornText")
	adornBuffer, err := adornText.GetBuffer()
	if err != nil {
		fmt.Println("Error: Failed to load buffer for adorn info TextView. Terminating program.")
		panic(err)
	}

	// Initialize search entry
	searchEntry := loadEntry("SearchEntry")

	// Initialize ListStore
	itemSelectList = loadListStore("ItemSelectList")

	// Initialize TreeView
	itemTreeView := loadTreeView("ItemTreeView")
	itemTreeView.Connect("cursor-changed", func() {
		selection, err := itemTreeView.GetSelection()
		if err != nil {
			fmt.Println("Error: Failed to get current selection.")
			return
		}
		model, iter, ok := selection.GetSelected()
		if !ok { // Occurs when item select window is reopened
			return
		}
		treeModel := model.(*gtk.TreeModel)
		vid, err := treeModel.GetValue(iter, 0) // Get ID
		if err != nil {
			fmt.Println("Error: Failed to get ID value from TreeModel.")
			return
		}
		uid, err := vid.GoValue()
		if err != nil {
			fmt.Println("Error: Failed to convert gvalue to govalue.")
			return
		}
		id, ok := uid.(int)
		if !ok {
			fmt.Println("Error: Failed to convert govalue to int.")
			return
		}
		selID = id
	})

	// Initialize buttons
	searchButton := loadButton("SearchButton")
	searchButton.Connect("clicked", func() {
		entry, err := searchEntry.GetText()
		if err != nil {
			fmt.Println("Error: Failed to get text from search entry.")
			return
		}
		if entry == "" {
			itemSelectList.Clear()
			for _, name := range allNames {
				var iter *gtk.TreeIter
				itemSelectList.InsertWithValues(iter, -1, []int{0, 1}, []interface{}{nameToID[name], name})
			}
			return
		}
		for entry[0] == ' ' {
			entry = entry[1:]
			if entry == "" {
				itemSelectList.Clear()
				for _, name := range allNames {
					var iter *gtk.TreeIter
					itemSelectList.InsertWithValues(iter, -1, []int{0, 1}, []interface{}{nameToID[name], name})
				}
				return
			}
		}
		entry = strings.ToUpper(entry) // ASCII matching is dependent on uppercase
		first := entry[0]
		if first < 48 || first > 90 || (first > 57 && first < 65) { // Invalid character
			itemSelectList.Clear()
			return
		}
		names := make([]string, 0)
		for _, id := range gear.AllDB[int(first)-48] {
			db := gear.ItemIndex[id]
			if db == "adorn" {
				if changeAdorn {
					adorn := gear.AdornDB[id]
					for _, slot := range adorn.Stats.Slots {
						if gear.SlotsO[curSlot] == slot {
							for _, slotType := range adorn.Stats.SlotTypes {
								if slotType == selSlot {
									names = append(names, gear.AdornDB[id].Info.Name)
								}
							}
						}
					}
				}
			} else {
				if db == "ammo" {
					if curSlot == 22 && !changeAdorn {
						names = append(names, gear.AmmoDB[id].Info.Name)
					}
				} else {
					if db == "armor" {
						if !changeAdorn {
							for _, slot := range gear.ArmorDB[id].Stats.Slots {
								if slot == gear.SlotsO[curSlot] {
									names = append(names, gear.ArmorDB[id].Info.Name)
									break
								}
							}
						}
					} else {
						if !changeAdorn {
							for _, slot := range gear.WeaponDB[id].Stats.Slots {
								if slot == gear.SlotsO[curSlot] {
									names = append(names, gear.WeaponDB[id].Info.Name)
									break
								}
							}
						}
					}
				}
			}
		}
		for i := range entry[1:] {
			removed := 0
			for j := 0; j < len(names)-removed; {
				if len(names[j]) < i+2 || strings.ToUpper(names[j])[i+1] != entry[i+1] {
					temp := names[j]
					names[j] = names[len(names)-1-removed]
					names[len(names)-1-removed] = temp
					removed++
				} else {
					j++
				}
			}
			names = names[:len(names)-removed]
		}
		sort.Slice(names, func(i, j int) bool { return names[i] < names[j] })
		itemSelectList.Clear()
		for _, name := range names {
			var iter *gtk.TreeIter
			itemSelectList.InsertWithValues(iter, -1, []int{0, 1}, []interface{}{nameToID[name], name})
		}
	})
	selectButton := loadButton("SelectItemButton")
	selectButton.Connect("clicked", func() {
		if selID == 0 {
			return
		}
		if !changeAdorn {
			characters.CurrentChar.Gear[curSlot].Status = true
			if curSlot < 19 {
				item := gear.ArmorDB[selID]
				characters.CurrentChar.Gear[curSlot].EquipA = item
				characters.CurrentChar.Gear[curSlot].Adorns = make([]gear.Adornment, len(item.Stats.SlotTypes))
				for i := range item.Stats.SlotTypes {
					characters.CurrentChar.Gear[curSlot].Adorns[i] = gear.Dummy
				}
				gearBuffer.SetText(characters.CurrentChar.Gear[curSlot].EquipA.Print())
				adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[curSlot], item.Stats))
			} else {
				if curSlot < 22 {
					db := gear.ItemIndex[selID]
					if db == "armor" {
						item := gear.ArmorDB[selID]
						characters.CurrentChar.Gear[curSlot].EquipA = item
						characters.CurrentChar.Gear[curSlot].EquipW.Info.ID = -1
						characters.CurrentChar.Gear[curSlot].Adorns = make([]gear.Adornment, len(item.Stats.SlotTypes))
						for i := range item.Stats.SlotTypes {
							characters.CurrentChar.Gear[curSlot].Adorns[i] = gear.Dummy
						}
						gearBuffer.SetText(characters.CurrentChar.Gear[curSlot].EquipA.Print())
						adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[curSlot], item.Stats))
					} else {
						item := gear.WeaponDB[selID]
						characters.CurrentChar.Gear[curSlot].EquipW = item
						characters.CurrentChar.Gear[curSlot].EquipA.Info.ID = -1
						characters.CurrentChar.Gear[curSlot].Adorns = make([]gear.Adornment, len(item.Stats.SlotTypes))
						for i := range item.Stats.SlotTypes {
							characters.CurrentChar.Gear[curSlot].Adorns[i] = gear.Dummy
						}
						gearBuffer.SetText(characters.CurrentChar.Gear[curSlot].EquipW.Print())
						adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[curSlot], item.Stats))
					}
				} else {
					characters.CurrentChar.Gear[curSlot].EquipAm = gear.AmmoDB[selID]
					gearBuffer.SetText(characters.CurrentChar.Gear[curSlot].EquipAm.Print())
				}
			}
		} else {
			if curSlot < 19 {
				characters.CurrentChar.Gear[curSlot].Adorns[adornIndex] = gear.AdornDB[selID]
				adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[curSlot], characters.CurrentChar.Gear[curSlot].EquipA.Stats))
			} else {
				if characters.CurrentChar.Gear[curSlot].EquipA.Info.ID != -1 {
					characters.CurrentChar.Gear[curSlot].Adorns[adornIndex] = gear.AdornDB[selID]
					adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[curSlot], characters.CurrentChar.Gear[curSlot].EquipA.Stats))
				} else {
					characters.CurrentChar.Gear[curSlot].Adorns[adornIndex] = gear.AdornDB[selID]
					adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[curSlot], characters.CurrentChar.Gear[curSlot].EquipW.Stats))
				}
			}
		}
		characters.WriteCharGear()
		itemSelectWindow.Hide()
	})
	cancelButton := loadButton("CancelItemButton")
	cancelButton.Connect("clicked", func() {
		itemSelectWindow.Hide()
	})
	removeItemButton := loadButton("RemoveItemButton")
	removeItemButton.Connect("clicked", func() {
		if !changeAdorn {
			characters.CurrentChar.Gear[curSlot].Status = false
		} else {
			if curSlot < 19 {
				characters.CurrentChar.Gear[curSlot].Adorns[adornIndex] = gear.Dummy
				adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[curSlot], characters.CurrentChar.Gear[curSlot].EquipA.Stats))
			} else {
				if characters.CurrentChar.Gear[curSlot].EquipA.Info.ID != -1 {
					characters.CurrentChar.Gear[curSlot].Adorns[adornIndex] = gear.Dummy
					adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[curSlot], characters.CurrentChar.Gear[curSlot].EquipA.Stats))
				} else {
					characters.CurrentChar.Gear[curSlot].Adorns[adornIndex] = gear.Dummy
					adornBuffer.SetText(printAdorn(characters.CurrentChar.Gear[curSlot], characters.CurrentChar.Gear[curSlot].EquipW.Stats))
				}
			}
		}
		itemSelectWindow.Hide()
	})
}

// loadAdornSelect connects the buttons in the adornment slot selection window to their respective functions.
func loadAdornSelect() {
	// Load ComboBoxText for selecting slot
	adornCombo := loadComboBoxText("AdornCombo")

	// Initialize buttons
	confirmButton := loadButton("ConfirmAdornButton")
	confirmButton.Connect("clicked", func() {
		slot, err := strconv.Atoi(adornCombo.GetActiveText())
		if err != nil {
			fmt.Printf("Error: Failed to convert %v to an integer.\n", adornCombo.GetActiveText())
			return
		}
		selSlot = slot
		getApplicableItems()
		itemSelectWindow.ShowAll()
		adornSelectWindow.Hide()
	})
	cancelButton := loadButton("CancelAdornButton")
	cancelButton.Connect("clicked", func() {
		adornSelectWindow.Hide()
	})
}

// getApplicableItems populates the allNames slice with the names of any items that meet the criteria set by the slot selected when the "Change Gear" button was pressed.
func getApplicableItems() {
	allNames = make([]string, 0)
	allIDs := make([]int, 0)
	for i := 0; i < 43; i++ {
		for _, id := range gear.AllDB[i] {
			db := gear.ItemIndex[id]
			if db == "adorn" {
				if changeAdorn {
					adorn := gear.AdornDB[id]
					for _, slot := range adorn.Stats.Slots {
						if gear.SlotsO[curSlot] == slot {
							for _, slotType := range adorn.Stats.SlotTypes {
								if slotType == selSlot {
									allNames = append(allNames, gear.AdornDB[id].Info.Name)
									allIDs = append(allIDs, id)
								}
							}
						}
					}
				}
			} else {
				if db == "ammo" {
					if curSlot == 22 && !changeAdorn {
						allNames = append(allNames, gear.AmmoDB[id].Info.Name)
						allIDs = append(allIDs, id)
					}
				} else {
					if db == "armor" {
						if !changeAdorn {
							for _, slot := range gear.ArmorDB[id].Stats.Slots {
								if slot == gear.SlotsO[curSlot] {
									allNames = append(allNames, gear.ArmorDB[id].Info.Name)
									allIDs = append(allIDs, id)
									break
								}
							}
						}
					} else {
						if !changeAdorn {
							for _, slot := range gear.WeaponDB[id].Stats.Slots {
								if slot == gear.SlotsO[curSlot] {
									allNames = append(allNames, gear.WeaponDB[id].Info.Name)
									allIDs = append(allIDs, id)
									break
								}
							}
						}
					}
				}
			}
		}
	}
	nameToID = make(map[string]int, 0)
	for i, name := range allNames {
		nameToID[name] = allIDs[i] // Here's to hoping you don't have gear pieces with identical names - shouldn't but some general items do
	}
	sort.Slice(allNames, func(i, j int) bool { return allNames[i] < allNames[j] })
	itemSelectList.Clear()
	for _, name := range allNames {
		var iter *gtk.TreeIter
		itemSelectList.InsertWithValues(iter, -1, []int{0, 1}, []interface{}{nameToID[name], name})
	}
}

// populateLists inserts all applicable (displayed) AA from the provided databases into the provided ListStores.
func populateLists(lists []*gtk.ListStore) {
	for i, list := range lists {
		for _, aa := range aa.DisplayedAA[i] {
			var iter *gtk.TreeIter
			list.InsertWithValues(iter, -1, []int{0, 1, 2, 3, 4, 5}, []interface{}{aa.ID, aa.Name, aa.Rank, aa.Level, aa.Cost, aa.Category})
		}
	}
}

// populateLists inserts all applicable (displayed) recipes from the provided databases into the provided ListStores.
func populateRecipeLists(lists []*gtk.ListStore) {
	for i, db := range ts.RecipeDBs {
		for _, recipe := range db {
			var iter *gtk.TreeIter
			lists[i].InsertWithValues(iter, -1, []int{0, 1, 2}, []interface{}{recipe.ID, recipe.Name, recipe.Trivial})
		}
	}
}

// clearList calls Clear() on a slice of ListStores provided to it.
func clearLists(lists []*gtk.ListStore) {
	for _, list := range lists {
		list.Clear()
	}
}

// printAdorn produces a string containing info on available slots and adornments in those slots, if any, suitable for display in a TextView.
func printAdorn(equipped gear.Equipped, item gear.Equippable) (text string) {
	for i, adorn := range equipped.Adorns {
		text += "Slot " + strconv.FormatInt(int64(item.SlotTypes[i]), 10) + ": "
		if adorn.Info.ID == 0 {
			text += "Empty\n"
		} else {
			text += adorn.Print()
		}
	}
	return
}

// getEntry returns the text from an Entry element, while providing error checking.
func getEntry(combo *gtk.Entry) (text string) {
	text, err := combo.GetText()
	if err != nil {
		fmt.Println("Error: Failed to get input from level buffer")
		return ""
	}
	return
}

// loadWindow returns a Window object, while providing error checking.
func loadWindow(id string) (window *gtk.Window) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: Window failed to initialize. Terminating program.")
		panic(err)
	}
	window, ok := object.(*gtk.Window)
	if !ok {
		fmt.Println("Error: Window failed to initialize. Terminating program.")
		panic(err)
	}
	return
}

// loadButton returns a Button object, while providing error checking.
func loadButton(id string) (button *gtk.Button) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: Button failed to initialize. Terminating program.")
		panic(err)
	}
	button, ok := object.(*gtk.Button)
	if !ok {
		fmt.Println("Error: Button failed to initialize. Terminating program.")
		panic(err)
	}
	return
}

// loadFileChooserDialog returns a FileChooserDialog object, while providing error checking.
func loadFileChooserDialog(id string) (fileChooserDialog *gtk.FileChooserDialog) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: File chooser failed to initialize. Terminating program.")
		panic(err)
	}
	fileChooserDialog, ok := object.(*gtk.FileChooserDialog)
	if !ok {
		fmt.Println("Error: File chooser failed to initialize. Terminating program.")
		panic(err)
	}
	return
}

// loadTextView returns a TextView object, while providing error checking.
func loadTextView(id string) (textView *gtk.TextView) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: TextView failed to initialize. Terminating program.")
		panic(err)
	}
	textView, ok := object.(*gtk.TextView)
	if !ok {
		fmt.Println("Error: TextView failed to initialize. Terminating program.")
		panic(err)
	}
	return
}

// loadTreeView returns a TreeView object, while providing error checking.
func loadTreeView(id string) (treeView *gtk.TreeView) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: TreeView failed to initialize. Terminating program.")
		panic(err)
	}
	treeView, ok := object.(*gtk.TreeView)
	if !ok {
		fmt.Println("Error: TreeView failed to initialize. Terminating program.")
		panic(err)
	}
	return
}

// loadListStore returns a ListStore object, while providing error checking.
func loadListStore(id string) (listStore *gtk.ListStore) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: ListStore failed to initialize. Terminating program.")
		panic(err)
	}
	listStore, ok := object.(*gtk.ListStore)
	if !ok {
		fmt.Println("Error: ListStore failed to initialize. Terminating program.")
		panic(err)
	}
	return
}

// loadToggleButton returns a ToggleButton object, while providing error checking.
func loadToggleButton(id string) (toggleButton *gtk.ToggleButton) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: ToggleButton failed to initialize. Terminating program.")
		panic(err)
	}
	toggleButton, ok := object.(*gtk.ToggleButton)
	if !ok {
		fmt.Println("Error: ToggleButton failed to initialize. Terminating program.")
		panic(err)
	}
	return
}

// loadEntry returns an Entry object, while providing error checking.
func loadEntry(id string) (entry *gtk.Entry) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: Entry failed to initialize. Terminating program.")
		panic(err)
	}
	entry, ok := object.(*gtk.Entry)
	if !ok {
		fmt.Println("Error: Entry failed to initialize. Terminating program.")
		panic(err)
	}
	return
}

// loadComboBoxText returns a ComboBoxText object, while providing error checking.
func loadComboBoxText(id string) (comboBoxText *gtk.ComboBoxText) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: ComboBoxText failed to initialize. Terminating program.")
		panic(err)
	}
	comboBoxText, ok := object.(*gtk.ComboBoxText)
	if !ok {
		fmt.Println("Error: ComboBoxText failed to initialize. Terminating program.")
		panic(err)
	}
	return
}

// loadMenuItem returns a MenuItem object, while providing error checking.
func loadMenuItem(id string) (menuItem *gtk.MenuItem) {
	object, err := builder.GetObject(id)
	if err != nil {
		fmt.Println("Error: MenuItem failed to initialize. Terminating program.")
		panic(err)
	}
	menuItem, ok := object.(*gtk.MenuItem)
	if !ok {
		fmt.Println("Error: MenuItem failed to initialize. Terminating program.")
		panic(err)
	}
	return
}
